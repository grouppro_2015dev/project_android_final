package thinhhv.pnv.com.managerment_login;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by thinhhv on 5/24/2015.
 */
public class CustomListViewAdapter extends ArrayAdapter<ListStudent>{

    Context context;

    public CustomListViewAdapter(Context context, int resource, List<ListStudent> objects) {
        super(context, resource, objects);
    }

//    public CustomListViewAdapter(Context context, int resource, List<Mark> objects) {
//        super(context, resource, objects);
//    }



//    public CustomListViewAdapter(Context context, int resource,List<Mark> items) {
//        super(context, resource, items);
//        this.context = context;
//    }

    private class ViewHolder {
        TextView txtid;
        TextView txtname;
        TextView txtage;
        TextView txtaddress;
        TextView txtcontact;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        ListStudent rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
//        if (convertView == null) {
//            convertView = mInflater.inflate(R.layout.list_item, null);
//            holder = new ViewHolder();
////            holder.txtid = (TextView) convertView.findViewById(R.id.txtname);
////            holder.txtname = (TextView) convertView.findViewById(R.id.txtname);
////            holder.txtage = (TextView) convertView.findViewById(R.id.txtage);
////            holder.txtaddress = (TextView) convertView.findViewById(R.id.txtaddress);
////            holder.txtcontact = (TextView) convertView.findViewById(R.id.txtcontact);
//
//
//            convertView.setTag(holder);
//        } else
//            holder = (ViewHolder) convertView.getTag();
//
//        holder.txtid.setText((int) rowItem.getId());
//        holder.txtname.setText(rowItem.getName());
//        holder.txtage.setText((int) rowItem.getAge());
//        holder.txtaddress.setText(rowItem.getAddress());
//        holder.txtcontact.setText(rowItem.getContact());

        return convertView;
    }


}
