package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/21/2015.
 */
public class ListStudent {
    private int id;
    private String name = "";
    private int age;
    private String address = "";
    private String contact = "";


    public ListStudent(int id, String name, int age, String address, String contact) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
        this.contact = contact;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
