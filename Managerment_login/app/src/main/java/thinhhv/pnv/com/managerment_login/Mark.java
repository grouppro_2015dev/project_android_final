package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/21/2015.
 */
public class Mark {
    private String name = "";
    private float mark1;
    private float mark2;
    private float markSum;
    private String comment = "";

    public Mark(String name, float mark1, float mark2, float markSum, String comment) {
        this.name = name;
        this.mark1 = mark1;
        this.mark2 = mark2;
        this.markSum = markSum;
        this.comment = comment;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getMark1() {
        return mark1;
    }

    public void setMark1(float mark1) {
        this.mark1 = mark1;
    }

    public float getMark2() {
        return mark2;
    }

    public void setMark2(float mark2) {
        this.mark2 = mark2;
    }

    public float getMarkSum() {
        return markSum;
    }

    public void setMarkSum(float markSum) {
        this.markSum = markSum;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
