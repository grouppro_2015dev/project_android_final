package thinhhv.pnv.com.managerment_login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MainActivity extends ActionBarActivity {
    EditText editTextUserName, editTextPassword;
    private Button btnGET;


    // Thanh
    ArrayAdapter<String> arrdapter;
    List arr = new ArrayList();
    List<ListTeacher> thanh = new ArrayList();

    private ProgressDialog pDialog;

    String ab;
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> productsList;



    private static String url_all_products = "http://192.168.103.24:8084/Project_Web/api/student/teacher/";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "";
    private static final String TAG_PID = "depCode";
    private static final String TAG_NAME = "depName";

    // products JSONArray
    JSONArray products = null;
    // Thanh

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        btnGET = (Button) findViewById(R.id.buttonSignIn);

//        btnGET.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                Toast.makeText(getApplicationContext(), "click get",
//                        Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(getApplicationContext(),
//                        AllProductsActivity.class);
//                startActivity(intent);
//            }
//        });


        //Thanh
        arr = new ArrayList<>();
//
//
//
        productsList = new ArrayList<HashMap<String, String>>();
//
//
        new LoadAllProducts().execute();
        //Thanh
    }


    // Thanh
    class LoadAllProducts extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();

            products = jParser.makeHttpRequesty(url_all_products, "GET", params1);
            JSONObject product = null;
            String duLieuName = "";

            // tao mang doi tuong ( voi moi doi tuong la 1 product)

            try {


//                for (int i = 0; i < products.length(); i++) {
                for (int j = 0; j < products.length(); j++) {
                    JSONArray dao = products.getJSONArray(j);
                    String name = dao.getString(0);
                    String mark1 = dao.getString(1);
                    String mark2 = dao.getString(2);
                    String markSum = dao.getString(3);
                    ListTeacher mark = new ListTeacher(name,mark1,mark2,markSum);
                    thanh.add(mark);
                    System.out.println("_______GET DATA:"+mark1);
//
                }
//

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  duLieuName;
        }






    }

    //Thanh


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public  void onButtonClick(View v)
    {
        editTextUserName = (EditText) findViewById(R.id.editTextUserNameToLogin);
        editTextPassword = (EditText) findViewById(R.id.editTextPasswordToLogin);
        String userName = editTextUserName.getText().toString();
        String password = editTextPassword.getText().toString();
        btnGET = (Button) findViewById(R.id.buttonSignIn);


        // bo doan ni.




    int kt = 0;
    // Ket thuc
        for(int i = 0; i < thanh.size(); i ++) {

            ListTeacher teacher = thanh.get(i);
            System.out.println("____GET DATA"+teacher.getMark1());
            if (userName.equals("") || password.equals("")) {
                System.out.println("KQ__1"+kt);
                kt = 0;
                System.out.println("KQ__1"+kt);
    //            Toast.makeText(getApplicationContext(), "Account Field ", Toast.LENGTH_LONG).show();
                return;
            }
            if (userName.equals(teacher.getMark1().toString()) && password.equals(teacher.getMark2().toString())) {
    //            Toast.makeText(MainActivity.this, "Account Successfully Created ", Toast.LENGTH_LONG).show();
    //            Intent intent = new Intent(MainActivity.this, Home.class);
    //            startActivity(intent);
                //  finish();
                System.out.println("KQ__2"+kt);
                kt = 1;
                System.out.println("KQ__2"+kt);
               break;
            } else {
                System.out.println("KQ__3"+kt);
                kt = 0;
                System.out.println("KQ__3"+kt);
            }
        }

        System.out.println("Thoát khỏi vòng for");
        if(kt == 1){
            System.out.println("Mật khẩu đúng");
            Toast.makeText(MainActivity.this, "Account Successfully Created ", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(MainActivity.this, Home.class);
            startActivity(intent);
        } else {
            System.out.println("Mật khẩu không đúng");
            Toast.makeText(getApplicationContext(), "User or Password Field ", Toast.LENGTH_LONG).show();
        }



//        if (v.getId() == R.id.buttonSignIn)
//        {
//
//            EditText a = (EditText)findViewById(R.id.editTextUserNameToLogin);
//            String str = a.getText().toString();
//
//            Intent i = new Intent(MainActivity.this, Display.class);
//            i.putExtra("Username", str);
//            startActivity(i);
//        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
