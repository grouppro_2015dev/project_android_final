package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/20/2015.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Java extends Activity {

    TextView tvDepName;
    private ProgressDialog pDialog;

    String ab;
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> productsList;



    private static String url_all_products = "http://192.168.103.67:8084/Project_Web/api/student/api?code=Web";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "";
    private static final String TAG_PID = "depCode";
    private static final String TAG_NAME = "depName";

    // products JSONArray
    JSONArray products = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.java);


        productsList = new ArrayList<HashMap<String, String>>();


        new LoadAllProducts().execute();


        tvDepName =(TextView) findViewById(R.id.btnjava);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 100) {

            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }

    String departmentName;
    class LoadAllProducts extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();

            products = jParser.makeHttpRequesty(url_all_products, "GET", params1);
            JSONObject product = null;
            String duLieuObject = "";

            // tao mang doi tuong ( voi moi doi tuong la 1 product)

            try {
                for (int i = 0; i < products.length(); i++) {

                        JSONArray dao = products.getJSONArray(0);
                        String id = dao.getString(0);
                        String name = dao.getString(1);
                        String age = dao.getString(2);
                        String subject = dao.getString(3);
                        String muoinam = dao.getString(4);
                        duLieuObject += id + "\n" + name + "\n" + age +"\n"+subject+
                                "\n"+muoinam+"\n"
                                +"--------------------------------"+ "\n";

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  duLieuObject;
        }

        protected void onPostExecute(String duLieu) {
            tvDepName.setText(duLieu);

        }

    }
}