package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/18/2015.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AllProductsActivity extends Activity {


    ListView lv;
    TextView txtid,txtname, txtmark1,txtmark2,txtmarksum;
    ArrayAdapter<String> arrdapter;
    List arr = new ArrayList();
    List<ListStudent> thanh = new ArrayList();

    private ProgressDialog pDialog;

    String ab;
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> productsList;

    private static String url_all_products = "http://192.168.103.67:8084/Project_Web/api/student/";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "";
    private static final String TAG_PID = "depCode";
    private static final String TAG_NAME = "depName";

    // products JSONArray
    JSONArray products = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_row_student);

        txtid = (TextView)findViewById(R.id.txtid);
        txtname = (TextView)findViewById(R.id.txtname);
        txtmark1 = (TextView)findViewById(R.id.txtage);
        txtmark2 = (TextView)findViewById(R.id.txtaddress);
        txtmarksum = (TextView)findViewById(R.id.txtcontact);

        lv = (ListView) findViewById(R.id.lvabc);
        arr = new ArrayList<>();



        productsList = new ArrayList<HashMap<String, String>>();


        new LoadAllProducts().execute();


//        tvDepName =(TextView) findViewById(R.id.btnplt);
        ListView all = (ListView) this.findViewById(R.id.lvabc);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 100) {

            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }

    String departmentName;
    class LoadAllProducts extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();

            products = jParser.makeHttpRequesty(url_all_products, "GET", params1);
            JSONObject product = null;
            String duLieuName = "";

            // tao mang doi tuong ( voi moi doi tuong la 1 product)

            try {
                for(int i = 0; i < products.length(); i++) {
                    product = products.getJSONObject(i);
                    int id = product.getInt("id");
                    String productName = product.getString("name");
                    int age = product.getInt("age");
                    String address = product.getString("address");
                    String contact = product.getString("contact");
                    ListStudent listStudent = new ListStudent(id,productName,age,address,contact);
                    thanh.add(listStudent);
//                        duLieuName += id + "\n" + productName + "\n" + age + "\n" + address + "\n" + contact + "\n"
//                                +"<--------------------------------------------------------->"+"\n";
                    arr.add(id +"\n"+"\n"+ "name': "+productName +"\n"+"\n"+"age: "
                            +age+"\n"+"\n"+ "address: "+address+"\n"+"\n"+"contact: "+contact);
//                    JSONArray dao = products.getJSONArray(i);
//                    float id = dao.getInt(Integer.parseInt("id"));
//                    String name = dao.getString(Integer.parseInt("name"));
//                    float age = dao.getInt(Integer.parseInt("age"));
//                    String address = dao.getString(Integer.parseInt("address"));
//                    String contact = dao.getString(Integer.parseInt("contact"));
//                    ListStudent mark = new ListStudent(id,name,age,address,contact);
//                    thanh.add(mark);
//                    arr.add("id: "+id + "name': "+name +"age: "+age+"address: "+address+"contact: "+contact);

                }

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        ListStudent mark_thanh = thanh.get(i);
                    txtname.setText(mark_thanh.getName());
                    String mark1 = mark_thanh.getAge()+"";
                    String mark2 = mark_thanh.getAddress()+"";
                    String marksum = mark_thanh.getContact()+"";
                    txtmark1.setText(mark1);
                    txtmark2.setText(mark2);
                    txtmarksum.setText(marksum);
                        Toast.makeText(AllProductsActivity.this, "View"+ mark_thanh.getName(), Toast.LENGTH_SHORT).show();
                    }
                });

//            }
//            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                    ListStudent mark_thanh = thanh.get(i);
//                    txtname.setText(mark_thanh.getName());
//                    String mark1 = mark_thanh.getAge()+"";
//                    String mark2 = mark_thanh.getAddress()+"";
//                    String marksum = mark_thanh.getContact()+"";
//                    txtmark1.setText(mark1);
//                    txtmark2.setText(mark2);
//                    txtmarksum.setText(marksum);
//                    Toast.makeText(AllProductsActivity.this, "View" + i, Toast.LENGTH_SHORT).show();
//                }
//            });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  duLieuName;
        }

        protected void onPostExecute(String duLieu) {
            arrdapter = new ArrayAdapter<String>(AllProductsActivity.this,android.R.layout.simple_list_item_1,arr);
            lv.setAdapter(arrdapter);

        }




    }
}