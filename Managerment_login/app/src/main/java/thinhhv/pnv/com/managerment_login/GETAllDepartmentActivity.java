package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/18/2015.
 */
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class GETAllDepartmentActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_student);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.getall_department, menu);
        return true;
    }

}
