package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/19/2015.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Teacher extends Activity {

    ListView lv;
    TextView txtname, txtmark1,txtmark2,txtmarksum;
    ArrayAdapter<String> arrdapter;
    List arr = new ArrayList();
    List<ListTeacher> thanh = new ArrayList();

    private ProgressDialog pDialog;

    String ab;
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> productsList;



    private static String url_all_products = "http://192.168.103.24:8084/Project_Web/api/student/teacher/";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "";
    private static final String TAG_PID = "depCode";
    private static final String TAG_NAME = "depName";

    // products JSONArray
    JSONArray products = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.teacher);

        txtname = (TextView)findViewById(R.id.txtname);
        txtmark1 = (TextView)findViewById(R.id.txtmark1);
        txtmark2 = (TextView)findViewById(R.id.txtmark2);
        txtmarksum = (TextView)findViewById(R.id.txtmarksum);

        lv = (ListView) findViewById(R.id.lvabc);
               arr = new ArrayList<>();



        productsList = new ArrayList<HashMap<String, String>>();


        new LoadAllProducts().execute();


//        tvDepName =(TextView) findViewById(R.id.btnplt);
        ListView all = (ListView) this.findViewById(R.id.lvabc);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 100) {

            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }

    String departmentName;
    class LoadAllProducts extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();

            products = jParser.makeHttpRequesty(url_all_products, "GET", params1);
            JSONObject product = null;
            String duLieuName = "";

            // tao mang doi tuong ( voi moi doi tuong la 1 product)

            try {


//                for (int i = 0; i < products.length(); i++) {
                    for (int j = 0; j < products.length(); j++) {
                        JSONArray dao = products.getJSONArray(j);
                        String name = dao.getString(0);
                        String mark1 = dao.getString(1);
                        String mark2 = dao.getString(2);
                        String markSum = dao.getString(3);
                        ListTeacher mark = new ListTeacher(name,mark1,mark2,markSum);
                        thanh.add(mark);
                        arr.add("Name: "+name +"\n"+"\n"+ "UserName: "+mark1 +"\n"+"\n"+"Password: "+mark2+"\n"+"\n"+ "Position: "+markSum);
                    }
//                    JSONArray thinh = products.getJSONArray(0);
//                    JSONArray thanh = products.getJSONArray(1);
//                    JSONArray dao = products.getJSONArray(2);
//                    JSONArray nhu = products.getJSONArray(3);
//                    String name = dao.getString(0);
//                    String uername = dao.getString(1);
//                    String password = dao.getString(2);
//                    String class_ = dao.getString(3);
//                    duLieuName += name + "\n"+uername+"\n"+password+"\n"+class_+"\n"+"--------------------------------"+ "\n";
//

//                }
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        ListTeacher mark_thanh = thanh.get(i);
                        txtname.setText(mark_thanh.getName());
                        String mark1 = mark_thanh.getMark1()+"";
                        String mark2 = mark_thanh.getMark2()+"";
                        String marksum = mark_thanh.getMarkSum()+"";
                        txtmark1.setText(mark1);
                        txtmark2.setText(mark2);
                        txtmarksum.setText(marksum);
                        Toast.makeText(Teacher.this, "View" + i, Toast.LENGTH_SHORT).show();
                    }
                });
//                for(int i = 0; i < products.length(); i++) {
//                    product = products.getJSONObject(i);
//                    int name = product.getInt("name");
//                    String username = product.getString(2);
//                    String password = product.getString("password");
//                    String class_ = product.getString("class");
//
//                    duLieuName += name + "\n" + username + "\n" + password + "\n" + class_ +"\n"
//                            +"<--------------------------------------------------------->"+"\n";
//
//
//                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  duLieuName;
        }

        protected void onPostExecute(String duLieu) {
            arrdapter = new ArrayAdapter<String>(Teacher.this,android.R.layout.simple_list_item_1,arr);
            lv.setAdapter(arrdapter);

        }




    }
}