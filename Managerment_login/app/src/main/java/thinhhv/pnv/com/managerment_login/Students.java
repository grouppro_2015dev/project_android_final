package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/18/2015.
 */
public class Students {
    private String id;
    private String depName;

    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String depCode) {
        this.depCode = depCode;
    }

    private String depCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }
}
