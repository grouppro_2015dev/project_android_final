package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/25/2015.
 */
public class ListTeacher {
    private String name = "";
    private String mark1 =  "";
    private String mark2;
    private String markSum = "";

    public ListTeacher(String name, String mark1, String mark2, String markSum) {
        this.name = name;
        this.mark1 = mark1;
        this.mark2 = mark2;
        this.markSum = markSum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMark1() {
        return mark1;
    }

    public void setMark1(String mark1) {
        this.mark1 = mark1;
    }

    public String getMark2() {
        return mark2;
    }

    public void setMark2(String mark2) {
        this.mark2 = mark2;
    }

    public String getMarkSum() {
        return markSum;
    }

    public void setMarkSum(String markSum) {
        this.markSum = markSum;
    }
}
