package thinhhv.pnv.com.managerment_login;

/**
 * Created by thinhhv on 5/20/2015.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class English extends Activity {

    ListView lv;
    TextView txtname, txtmark1,txtmark2,txtmarksum;
    ArrayAdapter<String> arrdapter;
    List arr = new ArrayList();
    List<Mark> thanh = new ArrayList();

    //    TextView tvDepName;
    private ProgressDialog pDialog;

    String ab;
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> productsList;



    private static String url_all_products = "http://192.168.103.67:8084/Project_Web/api/student/api?code=English";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "";
    private static final String TAG_PID = "depCode";
    private static final String TAG_NAME = "depName";

    // products JSONArray
    JSONArray products = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.english);

        txtname = (TextView)findViewById(R.id.txtname);
        txtmark1 = (TextView)findViewById(R.id.txtmark1);
        txtmark2 = (TextView)findViewById(R.id.txtmark2);
        txtmarksum = (TextView)findViewById(R.id.txtmarksum);

        lv = (ListView) findViewById(R.id.lvabc);
        arr = new ArrayList<>();



        productsList = new ArrayList<HashMap<String, String>>();


        new LoadAllProducts().execute();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 100) {

            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }

    String departmentName;
    class LoadAllProducts extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();

            products = jParser.makeHttpRequesty(url_all_products, "GET", params1);
            JSONObject product = null;
            String duLieuObject = "";

            // tao mang doi tuong ( voi moi doi tuong la 1 product)

            try {
//                for (int i = 0; i < products.length(); i++) {
                    for (int j = 0; j < products.length(); j++) {
                        JSONArray dao = products.getJSONArray(j);
                        String name = dao.getString(0);
                        float mark1 = dao.getInt(1);
                        float mark2 = dao.getInt(2);
                        float markSum = dao.getInt(3);
                        String comment = dao.getString(4);
                        Mark mark = new Mark(name,mark1,mark2,markSum,comment);
                        thanh.add(mark);
                        arr.add(name +"\n"+ "15': "+mark1 +" - "+"1h: "+mark2+" - "+ "Final: "+markSum);
                    }

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Mark mark_thanh = thanh.get(i);
                        txtname.setText(mark_thanh.getName());
                        String mark1 = mark_thanh.getMark1()+"";
                        String mark2 = mark_thanh.getMark2()+"";
                        String marksum = mark_thanh.getMarkSum()+"";
                        txtmark1.setText(mark1);
                        txtmark2.setText(mark2);
                        txtmarksum.setText(marksum);
                        Toast.makeText(English.this, "View" + i, Toast.LENGTH_SHORT).show();
                    }
                });
//                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  duLieuObject;
        }

        protected void onPostExecute(String duLieu) {
            arrdapter = new ArrayAdapter<String>(English.this,android.R.layout.simple_list_item_1,arr);
            lv.setAdapter(arrdapter);

        }

    }
}